using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddMemoryCache();
var conn = "Data Source=LAPTOP-NR588SC6\\SQLEXPRESS;Database=Caching;Trusted_Connection=True;TrustServerCertificate=True";
// todo register with dboption
builder.Services.AddDbContext<AppDbContext>(x => x.UseSqlServer(conn));

var app = builder.Build();

// Configure the HTTP request pipeline.

app.MapControllers();

app.Run();
