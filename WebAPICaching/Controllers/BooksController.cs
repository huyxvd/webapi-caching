using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Text.Json;

[ApiController]
[Route("[controller]/[action]")]
public class BooksController : ControllerBase
{
    private readonly AppDbContext _dbContext;
    private readonly IMemoryCache _memoryCache;
    private readonly string cacheKey = "mybooks";

    public BooksController(AppDbContext dbContext, IMemoryCache memoryCache)
    {
        _dbContext = dbContext;
        _memoryCache = memoryCache;
    }

    [HttpGet]
    public async Task<string> FakeData()
    {
        using StreamReader reader = new("books.json");
        var json = reader.ReadToEnd();
        var books = JsonSerializer.Deserialize<List<Book>>(json);
        await _dbContext.Books.AddRangeAsync(books);
        var count = await _dbContext.SaveChangesAsync();
        return $"success insert {count} record";
    }

    [HttpGet]
    public async Task<List<Book>> Books()
    {
        return await _dbContext.Books.ToListAsync();
    }

    [HttpGet]
    public async Task<List<Book>> Cache()
    {
        if (!_memoryCache.TryGetValue(cacheKey, out List<Book> books))
        {
            books = await _dbContext.Books.ToListAsync();

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromSeconds(10)); // expire in 10 second

            _memoryCache.Set(cacheKey, books, cacheEntryOptions);
        }

        return books;
    }
}
public class Book
{
    public int Id { get; set; }
    public string Title { get; set; }
}
